/*
Name : App.h
Author: Amit Malyala, Copyright Amit Malyala 2016- Current.
Description:
Header file for string processing function.
Version: 0.1
*/
#ifndef APP_H
#define APP_H
#include <string>
#include <vector>
#include <map>
#include "std_types.h"
#include "io.h"
#include <istream>
#include <sstream>
#include <chrono>
#include <iterator>
#include <algorithm>
/*
Component Function: void TextLineParser(const std::vector<io::DataContainer>& Text, std::vector <io::DataContainer>& WordList)
Arguments: String which is to be parsed, vector which stores all substrings in the string.
returns : None
Description
Function to detect words or substrings seperated by or ending in !@#$%^?&*()_|~",'.=+-[]{};:/ characters or 
numbers 0123456789 or other extended ascii characters.
Version and bug history : 
                          
 0.1 Initial version.
 0.2 Added support for word separators ,.:?-
 0.3 Corrected a bug which was parsing incorrectly at the end of message.
*/
/* Substring parsing code begins */
void TextLineParser(const std::vector<io::DataContainer>& Text, std::vector <io::DataContainer>& WordList);
/*
 Component Function: void StringProcessing(std::vector<io::DataContainer>& Text, std::vector <io::DataContainer>& WordList)
 Arguments:  Vector to process input and input file name.
 returns: None
 Description:
 Reads a ascii or UTF8 file into a vector.
 Version : 0.1
 Notes:
 Changed readfile to read lines in a file and detect matching string and line number of that string.
 
 To do:
 Read one or more strings in each line and push them into a vector .This vector would be used to insert 
 eacg strings and number of times, it occurs in file into a map.

*/
void StringProcessing(std::vector<io::DataContainer>& Text, std::vector <io::DataContainer>& WordList);

/*
 Component Function: BOOL isAlphabet(SCHAR8 Character)
 Arguments:  Character to be searched in list of alphabets
 Returns:
 Description: This function looks up all characters of a string in a list of alphabets a-z and A-Z
 Returns Alphabetic or not
  Version : 0.1
 */
BOOL isAlphabet(SCHAR8 Character);
/*
 Component Function: void ExecApplication(void)
 Arguments:  None
 returns: None
 Description:
 Executes application
 Version : 0.1
 Notes:
 To do:
 */
void ExecApplication(void);
#endif 