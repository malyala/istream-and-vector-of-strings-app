/*
Name : Input.cpp
Author: Amit Malyala, Copyright Amit Malyala 2016- Current.
Description:
Read input into a string.
Version: 0.1
*/
#include "Io.h"

/*
 Component Function: void io::ReadFile(std::vector <SCHAR8> &s,const SCHAR8* Fileptr)
 Arguments:  Vector to process input and input file name.
 returns: None
 Description:
 Reads a ascii or UTF8 file into a vector.
 Version : 0.1
 Notes:
 Changed readfile to read lines in a file.
*/
void io::ReadFile(std::vector <DataContainer> &s,const SCHAR8* Fileptr)
{
    BOOL EndofInput = false;
    std::ifstream myfile (Fileptr);
    std::string line;
    std::size_t LineNumber=1;
    io::DataContainer Dc;
    if (myfile.is_open())
    {
        while ((getline(myfile,line )) && EndofInput== false)  // For reading bytes
        {
            /*
               Read input until EOF
            */
            Dc.line=line;
            Dc.LineNumber=LineNumber;
            s.push_back(Dc);
            LineNumber++;
        }
        myfile.close();
    }
    else
    {
        std::cout << std:: endl << "File not found";
    }
}