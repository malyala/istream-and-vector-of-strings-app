/***************************************************
Name: istreamapp.cpp
Author: Amit Malyala.
Copyright (C) 2019 Amit Malyala, allrights reserved.
Description:
Write a function that, given an istream and a vector<string>, produces a map<string,vector<int> > holding each string and the numbers
of the lines on which the string appears. Run the program on a textfile with no fewer than 1,000 lines looking for no fewer than 10 words.

Change history:
0.1 Initial version

Notes:

To do:

Create a vector of string with text of English with each word separated by ' ' or other characters as ,;.!
We will use this vector for looking up input text strings. and print the number of occurences of each input string in the vector of strings.
The vector of string should be unique each time it is creaed from a text file.


Known issues:


Coding log:
10-11-19 - Created a baseline. Added IO module to read strings from a text file. Project compiles in ISO C++14 with G++ 4.9.2
           with -std=c++14 -Wall -Wpedantic  -fsanitize=leak settings.
12-11-19   Added Text parser. Created Line parser and string storage data structures.
14-11-19   Changed order of function calls.

***************************************************************************************/
#include "istreamapp.h"



/* Function main */
SINT32 main(void)
{
    ExecApplication();
    return 0;
}


/*
 Component Function: void ExecApplication(void)
 Arguments:  None
 returns: None
 Description:
 Executes application
 Version : 0.1
 Notes:
 To do:

 Known issues:
 */
void ExecApplication(void)
{
    std::vector <io::DataContainer> Text;
    std::map <std::string, std::vector<size_t> > myMap;
    std::vector <io::DataContainer> WordList;
    io::ReadFile(Text,"File.txt");
    BOOL input=true;
    StringProcessing(Text,WordList);
    std::cout << "Number of lines in text " << Text.size() << std::endl;
    std::cout << "Number of strings in vector: " << WordList.size() << std::endl;
    while (input!=false)
    {
        std::string word;
        std::cout << "Enter a word: ";
        std::getline(std::cin,word);
        std::vector <std::size_t> WordLineoccurence;
        //#if(0)
        std::size_t size=WordList.size();
        std::size_t LineNumber=0;
        for (std::size_t count=0; count<size; count++)
        {
            //std::cout << "Line:" << WordList[count].LineNumber << " " << WordList[count].line << std::endl;
            if ( word == WordList[count].line)
            {
                LineNumber=WordList[count].LineNumber;
                auto Found = std::find(std::begin(WordLineoccurence), std::end(WordLineoccurence), LineNumber);
                if (Found == std::end(WordLineoccurence))
                {
                    WordLineoccurence.push_back(LineNumber);
                    myMap[word].push_back(LineNumber);
                }
            }
        }
        if (word == "exit10")
        {
            input =false;
        }
        WordLineoccurence.clear();
    }
    //#endif

    std::cout << "Displaying map: " << std::endl;

    if (myMap.size())
    {
        for (auto &mm : myMap)
        {
            std::cout << "word \"" <<  mm.first << "\" found at:" << std::endl;
            for (std::size_t& a : mm.second)
            {
                std::cout << a << std::endl;
            }
            system("pause");

        }
    }
    else
    {
        std::cout << "No strings found" << std::endl;
    }

}

/*
 Component Function: void StringProcessing(std::vector<io::DataContainer>& Text, std::vector <io::DataContainer>& WordList)
 Arguments:  Vector to process input and input file name.
 returns: None
 Description:
 Reads a ascii or UTF8 file into a vector.
 Version : 0.1
 Notes:
 Changed readfile to read lines in a file and detect matching string and line number of that string.

 To do:
 known issues:
*/
void StringProcessing(std::vector<io::DataContainer>& Text, std::vector <io::DataContainer>& WordList)
{
    
    TextLineParser(Text,WordList);

    // Get char strings in each line from file which are in vector CurrentLineData.
}


/*
 Component Function: BOOL isAlphabet(SCHAR8 Character)
 Arguments:  Character to be searched in list of alphabets
 Returns:
 Description: This function looks up all characters of a string in a list of alphabets a-z and A-Z
 Returns Alphabetic or not
  Version : 0.1
 */
BOOL isAlphabet(SCHAR8 Character)
{
    return ((Character >= 'a' && Character <= 'z') || (Character >= 'A' && Character <= 'Z'));
}

/*
Component Function: void TextLineParser(const std::vector<io::DataContainer>& Text, std::vector <io::DataContainer>& WordList)
Arguments: String which is to be parsed, vector which stores all substrings in the string.
returns : None
Description
Function to detect words or substrings seperated by or ending in !@#$%^?&*()_|~",'.=+-[]{};:/ characters or
numbers 0123456789 or other extended ascii characters.

Notes:
This function extracts char strings from text file and builds a word list with a string and its line number.

To do:

Known issues:

Version and bug history :

 0.1 Initial version.
 0.2 Added support for word separators ,.:?-

*/

void TextLineParser(const std::vector<io::DataContainer>& Text, std::vector <io::DataContainer>& WordList)
{
    // Measure function execution time.
    // auto start = std::chrono::steady_clock::now();
    io::DataContainer substring { "",0};
    std::size_t count=0;
    std::size_t Linesize=0;
    WordList.clear();
    std::size_t NumberofLines=Text.size();
    std::size_t Linecount=0;
    // Change this loop to process each line at once than iterating with each char in all lines.
    while(Linecount < NumberofLines)
    {
        Linesize = Text[Linecount].line.size();
        count=0;
        while(count < Linesize && (Linesize))
        {
            //#if(0)
            /* This block is slightly slower than switch case statement in previous build by about 5 ms */
            if (std::isalpha(Text[Linecount].line[count]))
            {
                substring.line += Text[Linecount].line[count];
            }
            else
            {
                if (!substring.line.empty())
                {
                    substring.LineNumber=Text[Linecount].LineNumber;
                    // Add each extracted word to a vector
                    WordList.push_back(substring);
                    substring.line.erase();
                    substring.LineNumber=0;
                }
            }
            //#endif
            count++;
        }
        if (!substring.line.empty())
        {
            substring.LineNumber=Text[Linecount].LineNumber;
            // Add each extracted word to a vector
            WordList.push_back(substring);
            substring.line.erase();
            substring.LineNumber=0;
        }
        Linecount++;
    } // End of while
    // parsing code ends
    // Measure execution time.
    /*
    auto end = std::chrono::steady_clock::now();
    auto diff = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    std::cout << "Execution time " << diff << " milliseconds";
    */
}


