#ifndef IO_H
#define IO_H

#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include "std_types.h"
#include <cstddef>

namespace io
{

/* To store string and its line number */
struct DataContainer
{
    std::string line;
    std::size_t LineNumber;
};

/*
 Component Function: void ReadFile(std::vector <SCHAR8> &s,const SCHAR8* Fileptr)
 Arguments:  Vector to process input and input file name.
 returns: None
 Description:
 Reads a ascii or UTF8 file into a vector.
 Version : 0.1
 Notes:
 Changed readfile to read lines in a file.
*/
void ReadFile(std::vector <DataContainer> &s,const SCHAR8* Fileptr);
}

#endif