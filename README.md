# istream and vector of strings app

In this C++ project, given an istream and a vector<string>, it produces a map<string,vector<int> > holding each string and the numbers
of the lines on which the string appears. Run the program on a textfile with no fewer than 1,000 lines looking for no fewer than 10 words.

The vector<string> is constructed using a text file containing ascii text. 


Add "file.txt" which should contain more than a few thousand lines of english text into the build directory. 

This project compiles with G++ using ISO C++14.